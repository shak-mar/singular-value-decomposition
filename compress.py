#!/bin/python3

import argparse
import os

import PIL.Image as Image
import numpy as np
from numpy import linalg as la
import svd

import matplotlib.pyplot as plt

@np.vectorize
def c255to1(val):
    return val / 255.0

@np.vectorize
def c1to255(val):
    return abs(val) * 255.0

def save(matrix, outdir, name):
    Image.fromarray(c1to255(matrix)).convert("RGB").save("%s/%s.png" % (outdir,
        name))

parser = argparse.ArgumentParser(description="""Compute compression artifacts.
        Will write different levels of compression artifacts to
        outdir/reduced-*.png""")
parser.add_argument('image', help="""The image to apply Singular Value
        Decomposition to.  If it's not a grayscale image, only the green part
        will be used.""")
parser.add_argument('outdir', help='The directory to place outputs in')
parser.add_argument('--no-progress-bars', dest='progress', action='store_false',
        help='Hide progress bars (Default: show them)')
parser.add_argument('--progress-bars', dest='progress', action='store_true',
        help='Show progress bars (Default: show them)')
parser.set_defaults(progress=True)
parser.set_defaults(outdir='out')

if __name__ == '__main__':
    args = parser.parse_args()
    svd.progress_bars = args.progress
    image = Image.open(args.image)

    arr = np.array(image)
    if len(arr.shape) == 3:
        arr = arr[:,:,1]

    arr = c255to1(arr)

    if not os.path.isdir(args.outdir):
        os.mkdir(args.outdir)

    save(arr, args.outdir, "orig")

    A, P, Q, _ = svd.singular_value_decomposition(arr.copy())

    save(A, args.outdir, "A")
    save(P, args.outdir, "P")
    save(Q, args.outdir, "Q")

    reduced = np.zeros(A.shape)
    errors = []
    for i in range(min(arr.shape)):
        reduced += A[i, i] * np.outer(P[i], Q[:, i])
        errors.append(la.norm(arr - reduced, ord='fro'))
        print("Rank %d ~> frobenius norm of error: %f" % (i, errors[i]))
        save(reduced, args.outdir, "reduced-{:0>3}".format(i))
        if errors[i] < 5 and errors[i] == errors[i - 1]:
            break

    plt.plot(range(len(errors)), errors)
    plt.xlabel("Anzahl der Summanden (k)")
    plt.ylabel("Frobeniusnorm des Abstandes")
    plt.savefig("%s/errors.svg" % args.outdir)
